**_Сервис интегрирует в себе работу с двумя JAX-WS сервисами (Сбербанк-курс валют и GisMeteo-погода)_**

Используется Tomcat 9 + Servlet'ы в для прослушивания обращений к сервисам.

WSDL основного сервиса доступен по http://your-site/about_country?wsdl

WSDL погоды в столице доступен по http://your-site/weather?wsdl

WSDL курса валют доступен по http://your-site/courses?wsdl


В качестве БД используется MySQL и требует установку кодировки базы в UTF-8


Работа с базой происходит через Hibernate


Основанием для "распознания" столицы страны и её валюты - служит  XML файл который находится по пути _src/main/resources/countryRegistry.xml_

Файл заполняется вручную, поскольку нет сервиса который может сказать по названию страны её столицу и код валюты


Каждый запрос  и ответ со сторонего сервера логируется в базу, можно фильтровать в рамках сессии(Сессия - период от начала запроса до возврата ответа)

Каждый запрос имеет свой временой timestamp.


_Что стоит сделать_
Обработку данных.Рассмотреть случаи когда: 

    -входные данные неверны,
    
    -по данным нет совпадений в хранилище информации о странах
    
    -проблемы и перебои на стороне 3rd сервисов
    

