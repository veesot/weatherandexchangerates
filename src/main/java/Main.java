import org.apache.catalina.WebResourceRoot;
import org.apache.catalina.core.StandardContext;
import org.apache.catalina.startup.Tomcat;
import org.apache.catalina.webresources.StandardRoot;

import java.io.File;

/**
 * Created by  12.04.16.
 */
public class Main {

    public static void main(String[] args) throws Exception {

        String webappDirLocation = "src/main/webapp/";
        Tomcat tomcat = new Tomcat();
        tomcat.setPort(8080);

        StandardContext ctx = (StandardContext) tomcat.addWebapp("/", new File(webappDirLocation).getAbsolutePath());
        WebResourceRoot resources = new StandardRoot(ctx);
        ctx.setResources(resources);
        //Запустим сам tomcat чтобы подавал признаки жизни
        tomcat.start();
        tomcat.getServer().await();
    }
}
