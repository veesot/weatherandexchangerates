package service;


import cbr.client.GetCursOnDateXMLResponse.GetCursOnDateXMLResult;
import org.apache.commons.lang3.StringUtils;
import org.apache.xerces.dom.ElementNSImpl;
import org.apache.xerces.dom.TextImpl;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.math.BigDecimal;
import java.util.List;

/**
 * http://www.cbr.ru/scripts/Root.asp?PrtId=DWS
 * <p>
 * Описание методов сервиса:
 * GetCursOnDate(On_date) получение курсов валют на определенную дату (ежедневные курсы валют), GetSeldCursOnDate (ежемесячные курсы валют)
 * Аргументы:
 * On_date - Дата запроса для курсов, формат - System.DateTime
 * Результат:
 * XML документ в формате System.Data.Dataset, содержащий таблицу [ValuteCursOnDate],
 * таблица содержит поля:
 * <p>
 * Vname - Название валюты
 * Vnom - Номинал
 * Vcurs - Курс
 * Vcode - Цифровой код валюты
 * VchCode - Символьный код валюты
 */
public class GetCursOnDateAccessor {

    private static final GetCursOnDateAccessor instance = new GetCursOnDateAccessor();

    public static GetCursOnDateAccessor getInstance() {
        return instance;
    }

    private GetCursOnDateAccessor() {
    }

    /**
     * Vname - Название валюты
     * Vnom - Номинал
     * Vcurs - Курс
     * Vcode - Цифровой код валюты
     * VchCode - Символьный код валюты
     */
    public static class Currency {

        public String vname;
        public String vchCode;
        public Integer vcode;
        public BigDecimal vnom;
        public BigDecimal vcurs;

        @Override
        public String toString() {
            return String.format("Vname: %s, VchCode: %s, Vcode: %s, Vnom: %s, Vcurs: %s",
                    vname, vchCode, vcode.toString(), vnom.toString(), vcurs.toString()
            );
        }

    }


    public Currency getCurrencyByVchCode(String vchcode, GetCursOnDateXMLResult result) throws Exception {

        Currency currency = new Currency();

        List<Object> content = result.getContent();
        ElementNSImpl root = (ElementNSImpl) content.get(0);

        NodeList nodes = root.getElementsByTagName("VchCode");

        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            TextImpl text = (TextImpl) node.getFirstChild();
            String data = text.getData();

            if (StringUtils.equalsIgnoreCase(data, vchcode)) {
                initCurrency(currency, node);
                break; // <-- required node was found => leave loop
            }
        }

        return currency;

    }



    private void initCurrency(Currency currency, Node node) {

        Node parent = node.getParentNode();
        NodeList list = parent.getChildNodes();
        mangeNodeList(currency, list);

    }

    private void mangeNodeList(Currency currency, NodeList nodeList) {
        for (int j = 0; j < nodeList.getLength(); j++) {

            Node current = nodeList.item(j);

            String name = StringUtils.trimToEmpty(current.getNodeName());
            Node firstChild = current.getFirstChild();
            String value = StringUtils.trimToEmpty(firstChild.getNodeValue());

            switch (name) {
                case "Vname":
                    currency.vname = value;
                    break;
                case "Vnom":
                    currency.vnom = new BigDecimal(value);
                    break;
                case "Vcurs":
                    currency.vcurs = new BigDecimal(value);
                    break;
                case "Vcode":
                    currency.vcode = Integer.parseInt(value);
                    break;
                case "VchCode":
                    currency.vchCode = value;
                    break;
            }
        }

    }
}
