package service;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;
import java.io.File;
import java.io.IOException;


public class CountryParser {
    private Document doc;
    final String pathToCountryList = "src/main/resources/countryRegistry.xml";

    public CountryParser() throws SAXException, ParserConfigurationException, IOException {
        File inputFile = new File(pathToCountryList);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        doc = dBuilder.parse(inputFile);
    }

    public String getCurrencyByCountry(String country) throws Exception {
        XPath xpath = XPathFactory.newInstance().newXPath();
        String currency = xpath.evaluate(String.format("/countries/country[@name=\"%s\"]/currency/text()", country), doc.getDocumentElement());
        if (!currency.equals("")) { //Валюта найдена
            return currency;
        } else {
            return null;
        }
    }

    public String getCapitalByCountry(String country) throws Exception {
        XPath xpath = XPathFactory.newInstance().newXPath();
        String capital = xpath.evaluate(String.format("/countries/country[@name=\"%s\"]/capital/text()", country), doc.getDocumentElement());
        if (!capital.equals("")) {
            return capital;
        } else {
            return null;
        }
    }
}
