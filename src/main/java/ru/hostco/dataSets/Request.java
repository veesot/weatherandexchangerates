package ru.hostco.dataSets;

import javax.persistence.*;
import java.sql.Timestamp;


/**
 * veesot on 4/2/16.
 */
@Entity
@Table(name = "requests",uniqueConstraints=
@UniqueConstraint(columnNames = {"sessionUUID"}))
public class Request {
    private long requestId;
    private String sessionUUID;
    private String country;
    private Timestamp timestamp;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "request_id")
    public long getRequestId() {
        return requestId;
    }

    @SuppressWarnings("unused")
    public void setRequestId(long requestId) {
        this.requestId = requestId;
    }

    @Basic
    @Column(name = "sessionUUID")
    public String getSessionUUID() {
        return sessionUUID;
    }

    public void setSessionUUID(String sessionUUID) {
        this.sessionUUID = sessionUUID;
    }

    @Basic
    @Column(name = "country")
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Basic
    @Column(name = "timestamp")
    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }
}
