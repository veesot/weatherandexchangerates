package ru.hostco.dataSets;

import javax.persistence.*;
import java.sql.Timestamp;


/**
 * veesot on 4/2/16.
 */
@Entity
@Table(name = "responses")
public class Response {
    private long responseId;
    private String sessionUUID;
    private String country;
    private String answer;
    private String service;
    private Timestamp timestamp;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "response_id")
    public long getResponseId() {
        return responseId;
    }
    @SuppressWarnings("unused")
    public void setResponseId(long responseId) {
        this.responseId = responseId;
    }

    @Basic
    @Column(name = "sessionUUID")
    public String getSessionUUID() {
        return sessionUUID;
    }
    public void setSessionUUID(String sessionUUID) {
        this.sessionUUID = sessionUUID;
    }

    @Basic
    @Column(name = "country")
    public String getCountry() {
        return country;
    }
    public void setCountry(String country) {
        this.country = country;
    }

    @Basic
    @Column(name = "timestamp")
    public Timestamp getTimestamp() {
        return timestamp;
    }
    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    @Lob @Basic(fetch = FetchType.LAZY)
    @Column(name = "answer",length=10000000)
    public String getAnswer() {
        return answer;
    }
    public void setAnswer(String answer) {
        this.answer = answer;
    }

    @Basic
    @Column(name = "service")
    public String getService() {
        return service;
    }
    public void setService(String service) {
        this.service = service;
    }
}
