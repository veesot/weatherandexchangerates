package ru.hostco.dao;

import org.hibernate.cfg.Configuration;
import ru.hostco.dbService.DBService;

import java.io.Serializable;

public class AbstractDao implements Serializable {
    private DBService dbService;

    public void setConfiguration(Configuration configuration) {
        dbService = new DBService(configuration);
    }

    public <T> long save(T dataSet) {
        return dbService.save(dataSet);
    }

}
