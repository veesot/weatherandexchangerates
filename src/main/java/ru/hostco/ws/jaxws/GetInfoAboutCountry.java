
package ru.hostco.ws.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "getInfoAboutCountry", namespace = "http://ws.hostco.ru/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getInfoAboutCountry", namespace = "http://ws.hostco.ru/")
public class GetInfoAboutCountry {

    @XmlElement(name = "country", namespace = "")
    private String country;

    /**
     * 
     * @return
     *     returns String
     */
    public String getCountry() {
        return this.country;
    }

    /**
     * 
     * @param country
     *     the value for the country property
     */
    public void setCountry(String country) {
        this.country = country;
    }

}
