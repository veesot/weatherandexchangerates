package ru.hostco.ws;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.xml.ws.Action;


/**
 * veesot on 4/16/16.
 */
public interface ExchangeInfo {


    /**
     * @param country
     * @return returns java.lang.String
     */
    @WebMethod
    @WebResult(partName = "return")
    @Action(input = "http://ru.hostco.ws/ExchangeInfo/getRateToCurrencyRequest",
            output = "http://ru.hostco.ws/ExchangeInfo/getRateToCurrencyResponse")
    String getRateToCurrency(@WebParam(name = "country", partName = "country") String country) throws Exception;

}
