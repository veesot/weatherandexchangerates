package ru.hostco.ws;

/**
 * veesot on 4/16/16.
 */
public class WeatherConfig {
    private final String weatherIdx = "e851b2c8-56ec-4e55-84a2-9e225eb4a466"; // Уникальный ключ
    private final int count = 1; //Количество вариантов в ответе сервера,которые мы готовы принять
    private final String language = "RU"; //"Национальность" языка запросов.RU - мы задаем названия на русском языке

    protected String getWeatherIdx() {
        return this.weatherIdx;
    }

    protected int getCount() {
        return this.count;
    }

    protected String getLanguage() {
        return this.language;
    }
}
