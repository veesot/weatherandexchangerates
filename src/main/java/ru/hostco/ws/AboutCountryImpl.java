package ru.hostco.ws;

import org.hibernate.cfg.Configuration;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import ru.hostco.dao.AbstractDao;
import ru.hostco.dataSets.Request;
import ru.hostco.dataSets.Response;

import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.io.StringReader;
import java.io.StringWriter;
import java.sql.Timestamp;
import java.util.Date;
import java.util.UUID;

/**
 * veesot on 4/16/16.
 */
@WebService
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
public class AboutCountryImpl implements AboutCountry {
    private final AbstractDao dao;

    public AboutCountryImpl() {

        Configuration configuration = new Configuration().configure("hibernate.cfg.xml");

        this.dao = new AbstractDao();
        dao.setConfiguration(configuration);


    }

    @Override
    public String getInfoAboutCountry(@WebParam(name = "country", partName = "country") String country) throws Exception {
        String uuid = UUID.randomUUID().toString();//Идентифитатор сессии
        //Параметры запроса
        Request request = new Request();
        request.setCountry(country);
        request.setSessionUUID(uuid);
        request.setTimestamp(new Timestamp(new Date().getTime()));
        //Залогируем в БД
        dao.save(request);

        //Запрос на курс валют
        String aboutExchangeRates = new ExchangeInfoImpl().getRateToCurrency(country);
        //Логирование ответа от сервиса валют
        Response response = new Response();
        response.setCountry(country);
        response.setSessionUUID(uuid);
        response.setTimestamp(new Timestamp(new Date().getTime()));
        response.setAnswer(aboutExchangeRates);
        response.setService("ExchangeRates");
        dao.save(response);

        //Запрос на погоду в столице
        String aboutWeather = new WeatherImpl().getWeatherInCountryCapital(country);
        response.setCountry(country);
        response.setSessionUUID(uuid);
        response.setTimestamp(new Timestamp(new Date().getTime()));
        response.setAnswer(aboutWeather);
        response.setService("Weather");
        dao.save(response);



        // Соберём XML дерево которое вернём в ответ
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

        //Корень
        Document doc = docBuilder.newDocument();
        Element data = doc.createElement("data");
        doc.appendChild(data);
        Attr attr = doc.createAttribute("name");
        attr.setValue(String.format("%s", country));
        data.setAttributeNode(attr);

        //Подраздел с погодой
        Document weatherTree = DocumentBuilderFactory.newInstance().
                newDocumentBuilder().parse(new InputSource(new StringReader(aboutWeather)));
        //Выделим целевую ноду с инфорамцией о погоде
        XPath xpath = XPathFactory.newInstance().newXPath();
        Node tempNode = (Node)xpath.evaluate("/data/Weather",weatherTree, XPathConstants.NODE);
        Node weather = doc.importNode(tempNode,true);
        data.appendChild(weather);


        //Подраздел с курсами
        Document coursesTree = DocumentBuilderFactory.newInstance().
                newDocumentBuilder().parse(new InputSource(new StringReader(aboutExchangeRates)));
        //Выделим целевую ноду с инфорамцией о курсе
        tempNode = (Node)xpath.evaluate("/data/Courses",coursesTree, XPathConstants.NODE);
        Node courses = doc.importNode(tempNode,true);
        data.appendChild(courses);

        //Транформация в строку ответа
        DOMSource domSource = new DOMSource(doc);
        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        TransformerFactory tf = TransformerFactory.newInstance();
        tf.setAttribute("indent-number", 2);//Отступы вложеных тегов
        Transformer transformer = tf.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");//pretty XML print
        transformer.transform(domSource, result);
        writer.flush();

        return writer.toString();
    }
}
