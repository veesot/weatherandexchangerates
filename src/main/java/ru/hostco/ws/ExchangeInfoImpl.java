package ru.hostco.ws;

import cbr.client.DailyInfo;
import cbr.client.DailyInfoSoap;
import cbr.client.GetCursOnDateXMLResponse;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;
import service.CountryParser;
import service.GetCursOnDateAccessor;

import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.StringWriter;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * veesot on 4/16/16.
 */
@WebService
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
public class ExchangeInfoImpl implements ExchangeInfo {
    @Override
    public String getRateToCurrency(@WebParam(name = "country", partName = "country") String country) throws Exception {
        //Найдём валюту в XML-хранилище
        CountryParser countryParser = new CountryParser();
        String currency_name = countryParser.getCurrencyByCountry(country);

        //Строка ответа от сервиса
        StringWriter writer = new StringWriter();

        //Сборка ответа от сервиса валют
        DocumentBuilderFactory currencyFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder currencyBuilder = currencyFactory.newDocumentBuilder();
        Document currencyTree = currencyBuilder.newDocument();

        //Корневой элемент
        Element root = currencyTree.createElement("data");
        //Метаданные о курсах
        Element Courses = currencyTree.createElement("Courses");
        //Подраздел c  информацией о валюте
        Element ValuteCursOnDate = currencyTree.createElement("ValuteCursOnDate");
        Courses.appendChild(ValuteCursOnDate);


        if (currency_name != null) {
            //Текущая дата в грегорианском стиле
            GregorianCalendar gregorianCalendar = new GregorianCalendar();
            gregorianCalendar.setTime(new Date());
            DatatypeFactory datatypeFactory = DatatypeFactory.newInstance();
            XMLGregorianCalendar date = datatypeFactory.newXMLGregorianCalendar(gregorianCalendar);

            DailyInfoSoap service = new DailyInfo().getDailyInfoSoap();//Мета инфорамция от сервиса

            GetCursOnDateXMLResponse.GetCursOnDateXMLResult result = service.getCursOnDateXML(date);
            GetCursOnDateAccessor accessor = GetCursOnDateAccessor.getInstance();

            //Запрашиваем по коду валюты(USD,EUR...)
            GetCursOnDateAccessor.Currency currency = accessor.getCurrencyByVchCode(currency_name, result);

            if (currency.vcurs != null) {//Бывает такое что источник ничего не знает о нашей валюте

                //Создадим вложеные элементы характеризующие курс на текущую дату

                //Valute name
                Element element = currencyTree.createElement("Vname");
                Text text = currencyTree.createTextNode(currency.vname);
                element.appendChild(text);
                ValuteCursOnDate.appendChild(element);

                //Valute nominal
                element = currencyTree.createElement("Vnom");
                text = currencyTree.createTextNode(String.valueOf(currency.vnom));
                element.appendChild(text);
                ValuteCursOnDate.appendChild(element);

                //Valute curs
                element = currencyTree.createElement("Vcurs");
                text = currencyTree.createTextNode(String.valueOf(currency.vcurs));
                element.appendChild(text);
                ValuteCursOnDate.appendChild(element);

                //Valute code
                element = currencyTree.createElement("Vcode");
                text = currencyTree.createTextNode(String.valueOf(currency.vcode));
                element.appendChild(text);
                ValuteCursOnDate.appendChild(element);

                //Valute country letter code
                element = currencyTree.createElement("VchCode");
                text = currencyTree.createTextNode(currency.vchCode);
                element.appendChild(text);
                ValuteCursOnDate.appendChild(element);

                root.appendChild(Courses);
                currencyTree.appendChild(root);


                //Транформация в строку ответа
                DOMSource domSource = new DOMSource(currencyTree);

                StreamResult resultXML = new StreamResult(writer);
                TransformerFactory tf = TransformerFactory.newInstance();
                tf.setAttribute("indent-number", 2);//Отступы вложеных тегов
                Transformer transformer = tf.newTransformer();
                transformer.setOutputProperty(OutputKeys.INDENT, "yes");//pretty XML print
                transformer.transform(domSource, resultXML);
                writer.flush();
            }
        }


        return writer.toString();

    }
}
