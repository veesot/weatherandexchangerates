package ru.hostco.ws;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;
import ru.gismeteo.ws.*;
import service.CountryParser;

import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.StringWriter;
import java.util.List;


/**
 * veesot on 4/16/16.
 */
@WebService
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
public class WeatherImpl implements Weather {

    @Override
    public String getWeatherInCountryCapital(@WebParam(name = "country", partName = "country") String country) throws Exception {
        //Все настройки для запросов
        WeatherConfig weatherConfig = new WeatherConfig();
        String weatherIdx = weatherConfig.getWeatherIdx();
        int count = weatherConfig.getCount();
        String language = weatherConfig.getLanguage();

        //Найдём столицу страны в XML-хранилище
        CountryParser countryParser = new CountryParser();
        String capital = countryParser.getCapitalByCountry(country);


        //Узнаем идентификатор столицы
        LocationsSoap locationsSoap = new Locations().getLocationsSoap();
        LocationInfoFullResult locationInfoFullResult = locationsSoap.findByNameFull(weatherIdx, capital, count, language);
        ArrayOfLocationInfoFull result = locationInfoFullResult.getData();

        //Нас интересует идентификатор первого (и единственного) города в выборке
        int cityIdx = result.getLocationInfoFull().get(0).getId();

        //После чего мы можем сходить и узнать по нему погоду
        WeatherSoap weatherSoap = new ru.gismeteo.ws.Weather().getWeatherSoap();
        HHForecastResult hhForecastResult = weatherSoap.getHHForecast(weatherIdx, cityIdx);
        ArrayOfHHForecast forecastList = hhForecastResult.getData();



        //Преобразуем форекаст от сервиса в ноду для дерева.Прямое кастование не сработает.
        Document weatherTree = this.convertForecastListToNode(forecastList.getHHForecast(),capital);


        //Транформация в строку ответа
        DOMSource domSource = new DOMSource(weatherTree);
        StringWriter writer = new StringWriter();
        StreamResult streamResult = new StreamResult(writer);
        TransformerFactory tf = TransformerFactory.newInstance();
        tf.setAttribute("indent-number", 2);//Отступы вложеных тегов
        Transformer transformer = tf.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");//pretty XML print
        transformer.transform(domSource, streamResult);
        writer.flush();

        return writer.toString();


    }

    private Document convertForecastListToNode(List<HHForecast> forecastList,String capital) throws ParserConfigurationException, JAXBException {
        DocumentBuilderFactory forecastFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder weatherBuilder = forecastFactory.newDocumentBuilder();
        Document weatherTree = weatherBuilder.newDocument();

        //Корневой элемент
        Element root = weatherTree.createElement("data");
        //Метаданные о погоде
        Element Weather = weatherTree.createElement("Weather");
        Attr attr = weatherTree.createAttribute("name");
        attr.setValue(String.format("%s", capital));//Информация о столице
        Weather.setAttributeNode(attr);

        for (HHForecast forecast : forecastList) {

            Element HHForecast = weatherTree.createElement("HHForecast");

            Element element  = weatherTree.createElement("time");
            Text text = weatherTree.createTextNode(forecast.getTime().toString());
            element.appendChild(text);
            HHForecast.appendChild(element);

            element  = weatherTree.createElement("tod");
            text = weatherTree.createTextNode(String.valueOf(forecast.getTod()));
            element.appendChild(text);
            HHForecast.appendChild(element);

            element  = weatherTree.createElement("t");
            text = weatherTree.createTextNode(String.valueOf(forecast.getT()));
            element.appendChild(text);
            HHForecast.appendChild(element);

            element  = weatherTree.createElement("p");
            text = weatherTree.createTextNode(String.valueOf(forecast.getP()));
            element.appendChild(text);
            HHForecast.appendChild(element);

            element  = weatherTree.createElement("cl");
            text = weatherTree.createTextNode(String.valueOf(forecast.getCl()));
            element.appendChild(text);
            HHForecast.appendChild(element);

            element  = weatherTree.createElement("prc");
            text = weatherTree.createTextNode(String.valueOf(forecast.getPrc()));
            element.appendChild(text);
            HHForecast.appendChild(element);

            element  = weatherTree.createElement("prct");
            text = weatherTree.createTextNode(String.valueOf(forecast.getPrct()));
            element.appendChild(text);
            HHForecast.appendChild(element);

            element  = weatherTree.createElement("dd");
            text = weatherTree.createTextNode(String.valueOf(forecast.getDd()));
            element.appendChild(text);
            HHForecast.appendChild(element);

            element  = weatherTree.createElement("ff");
            text = weatherTree.createTextNode(String.valueOf(forecast.getFf()));
            element.appendChild(text);
            HHForecast.appendChild(element);

            element  = weatherTree.createElement("st");
            text = weatherTree.createTextNode(String.valueOf(forecast.getSt()));
            element.appendChild(text);
            HHForecast.appendChild(element);

            element  = weatherTree.createElement("humidity");
            text = weatherTree.createTextNode(String.valueOf(forecast.getHumidity()));
            element.appendChild(text);
            HHForecast.appendChild(element);


            Weather.appendChild(HHForecast);
        }
        root.appendChild(Weather);
        weatherTree.appendChild(root);

        return weatherTree;
    }

}
