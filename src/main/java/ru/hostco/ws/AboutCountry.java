package ru.hostco.ws;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.Action;

/**
 * veesot on 4/16/16.
 */
@WebService(endpointInterface = "ru.hostco.ws.AboutCountry")
public interface AboutCountry {
    /**
     * @param country
     * @return returns java.lang.String
     */
    @WebMethod
    @WebResult(partName = "return")
    @Action(input = "http://ru.hostco.ws/AboutCountry/getInfoAboutCountryRequest",
            output = "http://ru.hostco.ws/AboutCountry/getInfoAboutCountryResponse")
    String getInfoAboutCountry(@WebParam(name = "country", partName = "country") String country) throws Exception;
}
