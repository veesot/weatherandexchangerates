package ru.hostco.dbService;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.service.ServiceRegistry;


public class DBService {
    private Class clazz;


    private final SessionFactory sessionFactory;


    public DBService(Configuration configuration) {
        sessionFactory = createSessionFactory(configuration);
    }


    private static SessionFactory createSessionFactory(Configuration configuration) {
        StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder();
        builder.applySettings(configuration.getProperties());
        ServiceRegistry serviceRegistry = builder.build();
        return configuration.buildSessionFactory(serviceRegistry);
    }



    public <T> long save(T dataSet) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();
        try {
            long id = (Long) session.save(dataSet);
            tx.commit();
            return id;
        } catch (ConstraintViolationException e) {//Случай с дубликатами записи
            tx.rollback();
            return 0;
        } finally {
            session.close();
        }
    }


}
